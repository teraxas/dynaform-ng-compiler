package org.dynaform.json;

import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.dynaform.model.Form;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Java string escaper http://www.buildmystring.com/default.php
 *
 * @author maksim.pecorin@seb.se, 11/09/2017.
 */
public class FormJsonUtilsTest {
	private static Gson GSON;

	@BeforeClass
	public static void before() {
		GSON = new Gson();
	}

	@Test
	public void testFormJsonHandlesPolymorphicDeserializationWell() {
		String json = formJson();

		Form form = FormJsonUtils.deserialize(json, Form.class);

		assertEquals(1, form.controls.size());

		Form.Container topContainer = (Form.Container) form.controls.get(0);
		assertEquals(Form.Container.class, topContainer.getClass());
		assertEquals(3, topContainer.controls.size());

		Form.FormControl control0 = topContainer.controls.get(0);
		assertEquals("Dropdown", control0.type);
		assertEquals(Form.FieldOptions.class, control0.getClass());

		Form.FormControl control1 = topContainer.controls.get(1);
		assertEquals("Dropdown", control1.type);
		assertEquals(Form.FieldOptions.class, control1.getClass());

		Form.FormControl control2 = topContainer.controls.get(2);
		assertEquals("Date", control2.type);
		assertEquals(Form.Field.class, control2.getClass());
	}

	@Test
	public void testFormJsonHandlesPolymorphismSerializationWell() {
		String json = formJson();
		Form form = FormJsonUtils.deserialize(json, Form.class);
		String json2 = FormJsonUtils.serialize(form);

		assertJsonEquals(json, json2);
	}

	private String formJson() {
		return "{" +
				"  \"controls\": [" +
				"    {" +
				"      \"controls\": [" +
				"        {" +
				"          \"classifier\": [" +
				"            {" +
				"              \"displayValue\": \"ISDA 92\"," +
				"              \"value\": \"ISDA92\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"ISDA 02\"," +
				"              \"value\": \"ISDA02\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"ISDA DF 13\"," +
				"              \"value\": \"ISDADF13\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"CSA 2016 ISDA 02\"," +
				"              \"value\": \"CSA2016_ISDA02\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"CSA 2016 ISDA 92\"," +
				"              \"value\": \"CSA2016_ISDA92\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"CSA 2016 ISDA DF 13\"," +
				"              \"value\": \"CSA2016_ISDADF13\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"CSA ISDA 02\"," +
				"              \"value\": \"CSA_ISDA02\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"CSA ISDA 92\"," +
				"              \"value\": \"CSA_ISDA92\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"CSA ISDA DF 13\"," +
				"              \"value\": \"CSA_ISDADF13\"" +
				"            }" +
				"          ]," +
				"          \"label\": \"Document type\"," +
				"          \"name\": \"docType\"," +
				"          \"type\": \"Dropdown\"," +
				"          \"validators\": []" +
				"        }," +
				"        {" +
				"          \"classifier\": [" +
				"            {" +
				"              \"displayValue\": \"Active\"," +
				"              \"value\": \"ACTIVE\"" +
				"            }," +
				"            {" +
				"              \"displayValue\": \"Rejected\"," +
				"              \"value\": \"REJECTED\"" +
				"            }" +
				"          ]," +
				"          \"label\": \"Status\"," +
				"          \"name\": \"status\"," +
				"          \"type\": \"Dropdown\"," +
				"          \"validators\": []" +
				"        }," +
				"        {" +
				"          \"label\": \"Master Agreement Date\"," +
				"          \"name\": \"masterAgreementDate\"," +
				"          \"type\": \"Date\"," +
				"          \"validators\": []" +
				"        }" +
				"      ]," +
				"      \"horizontal\": false," +
				"      \"label\": \"Report criteria\"," +
				"      \"name\": \"reportCriteria\"," +
				"      \"optional\": false," +
				"      \"type\": \"Container\"," +
				"      \"validators\": []" +
				"    }" +
				"  ]," +
				"  \"formHandlers\": []," +
				"  \"formName\": \"Test report #1 for unit tests\"," +
				"  \"formSymbolicName\": \"TestQuery1\"" +
				"}";
	}

	private void assertJsonEquals(String expectedJson, String actualJson) {
		Map expectedJsonMap = GSON.fromJson(expectedJson, Map.class);
		Map actualJsonMap = GSON.fromJson(actualJson, Map.class);

		MapDifference difference = Maps.difference(expectedJsonMap, actualJsonMap);
		assertTrue(difference.areEqual());
	}

	@Test
	public void testDatesAreIso() {
		DateHolder dateHolder = new DateHolder(new DateTime(2017, 9, 11, 0, 0, DateTimeZone.UTC).toDate());

		String json = FormJsonUtils.serialize(dateHolder);

		DateHolder dateHolder2 = FormJsonUtils.deserialize(FormJsonUtils.serialize(dateHolder), DateHolder.class);

		assertTrue(json.contains("2017-09-11T"));
		assertEquals(dateHolder, dateHolder2);
	}

	private static class DateHolder {
		Date date;

		public DateHolder(Date someDate) {
			this.date = someDate;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			DateHolder that = (DateHolder) o;

			return date != null ? date.equals(that.date) : that.date == null;

		}

		@Override
		public int hashCode() {
			return date != null ? date.hashCode() : 0;
		}
	}
}