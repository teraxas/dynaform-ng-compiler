package org.dynaform.compiler;

import com.google.common.base.Function;
import com.google.common.base.Verify;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.dynaform.model.Form;
import org.dynaform.model.Classifier;
import org.dynaform.model.FormControlType;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Finds {@link Form.FormControl} with external {@link Classifier} links
 * and replaces them with direct classifier definitions in options field.
 */
public class FormCompiler {

	private FormProvider formProvider;

	public FormCompiler(FormProvider formProvider) {
		this.formProvider = formProvider;
	}

	public Form get(String formId) {
		Form form = formProvider.getForm(formId);
		replaceClassifiers(form.controls);
		return form;
	}

	private void replaceClassifiers(List<Form.FormControl> controls) {
		List<Form.FormControl> toReplace = Lists.newArrayList();

		for (Form.FormControl fc : controls) {
			if (FormControlType.REPLACEABLE_FIELD_NAMES.contains(fc.type)) {
				Verify.verify(fc instanceof Form.HasClassifierId, "Replaceable FormControl must implement HasClassifierId: " + fc.type);
				toReplace.add(fc);
			} else if (FormControlType.CONTAINER_NAMES.contains(fc.type)) {
				replaceClassifiers(((Form.Container) fc).controls);
			}
		}

		executeReplacement(controls, toReplace);
	}

	private void executeReplacement(List<Form.FormControl> controls, List<Form.FormControl> toReplace) {
		for (Form.FormControl control : toReplace) {
			int index = controls.indexOf(control);
			String classifierId = ((Form.HasClassifierId) control).getClassifierId();
			Classifier classifier = this.formProvider.getClassifier(classifierId);

			Form.FormControl replacement = FormControlType.getByTypeName(control.type).getReplacedBy().getNew();
			Verify.verify(replacement instanceof Form.HasOptions, "Replacement FormControl must implement HasOptions: " + replacement.type);

			mapMatching(control, replacement);
			((Form.HasOptions) replacement).setOptions(classifier);

			controls.remove(index);
			controls.add(index, replacement);
		}
	}

	private void mapMatching(Form.FormControl from, Form.FormControl to) {
		Field[] toFields = to.getClass().getFields();
		HashSet<Field> fromFields = Sets.newHashSet(from.getClass().getFields());
		Collection<String> fromFieldNames = Collections2.transform(fromFields, new Function<Field, String>() {
			@Override
			public String apply(Field field) {
				return field.getName();
			}
		});

		for (Field f : toFields) {
			if (fromFieldNames.contains(f.getName())) {
				try {
					f.set(to, from.getClass().getField(f.getName()).get(from));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

}
