package org.dynaform.compiler;

import org.dynaform.model.Classifier;
import org.dynaform.model.Form;

/**
 * Interface for providing loaded form JSONs to FormCompiler
 */
public interface FormProvider {

	public Classifier getClassifier(String classifierId);

	/**
	 * Should provide a new instance as it will mutate if used in {@link FormCompiler}
	 */
	public Form getForm(String formId);

}
