package org.dynaform.compiler;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.nio.charset.Charset;

public class ConversionUtils {

	public static String fileToString(String filename) {
		try {
			ClassLoader classLoader = ConversionUtils.class.getClassLoader();
			File file = new File(classLoader.getResource(filename).getFile());
			return Files.toString(file, Charset.forName("UTF-8"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static JsonObject toJsonObject(String string) {
		return getGson().fromJson(string, JsonElement.class).getAsJsonObject();
	}

	public static Gson getGson() {
		return new Gson();
	}
}
