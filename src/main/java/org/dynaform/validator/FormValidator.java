package org.dynaform.validator;

import com.google.common.base.Verify;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.dynaform.compiler.ConversionUtils;
import org.dynaform.model.FormControlType;

/**
 * Created by S86571 on 2017.08.24.
 */
public class FormValidator {

	private static final String ERR_MISSING_CONTROL_INFO = "Missing FormControl base information ";
	private static final String ERR_MISSING_CONTROLS = "Missing array of 'controls'";
	private static final String ERR_UNEXPECTED_CONTROL_TYPE = "FormControl.type is not set OR not in FIELDS or CONTAINERS at FormControlType: ";
	private static final String ERR_MISSING_FORM_INFO = "Missing form info";

	public void validate(String formJsonString) {
		validate(ConversionUtils.toJsonObject(formJsonString));
	}

	public void validate(JsonObject formJsonObject) {
		Verify.verify(formJsonObject.has("formSymbolicName"), ERR_MISSING_FORM_INFO);
		Verify.verify(formJsonObject.has("formName"), ERR_MISSING_FORM_INFO);
		Verify.verify(formJsonObject.has("formHandlers"), ERR_MISSING_FORM_INFO);
//		Verify.verify(formJsonObject.has("version"), ERR_MISSING_FORM_INFO);
		validateControlls(formJsonObject);
	}

	private void validateControlls(JsonObject jsonObject) {
		JsonElement controllsElement = jsonObject.get("controls");
		Verify.verify(controllsElement != null && controllsElement.isJsonArray(), ERR_MISSING_CONTROLS);
		JsonArray controlls = controllsElement.getAsJsonArray();

		for (JsonElement e: controlls) {
			JsonObject control = e.getAsJsonObject();
			String type = control.get("type").getAsString();
			validateControl(control);

			if (FormControlType.FIELD_NAMES.contains(type)) {
				validateField(control);
			} else if (FormControlType.CONTAINER_NAMES.contains(type)) {
				validateContainer(control);
			} else {
				throw new ValidationException(ERR_UNEXPECTED_CONTROL_TYPE + control);
			}
		}
	}

	private void validateControl(JsonObject control) {
		Verify.verify(control.has("name"), ERR_MISSING_CONTROL_INFO + control);
		Verify.verify(control.has("label"), ERR_MISSING_CONTROL_INFO + control);
	}

	private void validateContainer(JsonObject container) {
		validateControlls(container);
		// TODO
	}

	private void validateField(JsonObject field) {
		// TODO
	}

	private class ValidationException extends RuntimeException {
		public ValidationException(String s) {
			super(s);
		}
	}
}
