package org.dynaform.json;

import com.google.gson.*;
import org.apache.commons.lang3.StringUtils;
import org.dynaform.model.Form;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.dynaform.model.FormControlType;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Map;

import static java.lang.String.format;

public abstract class FormJsonUtils {
	private static Map<String, Class> CONTROLTYPE_CLASSES_MAP = FormControlType.getControlTypesToClassesMap();

	private static final Gson gson = buildGson().create();

	private static GsonBuilder buildGson() {
		final GsonBuilder gsonBuilder = new GsonBuilder();

		gsonBuilder.registerTypeAdapter(Form.FormControl.class, new FormControlAdapter());
		gsonBuilder.registerTypeAdapter(Date.class, new DateAdapter());
		return gsonBuilder;
	}

	private FormJsonUtils() {
	}

	public static String serialize(Object obj) {
		return gson.toJson(obj);
	}

	public static <T> T deserialize(String json, Class<T> clazz) {
		return gson.fromJson(json, clazz);
	}

	private static class FormControlAdapter implements JsonSerializer<Form.FormControl>, JsonDeserializer<Form.FormControl> {
		@Override
		public JsonElement serialize(
				Form.FormControl obj,
				Type typeOfSrc,
				JsonSerializationContext context
		) {
			return context.serialize(obj);
		}

		public Form.FormControl deserialize(
				JsonElement json,
				Type typeOfT,
				JsonDeserializationContext context
		) throws JsonParseException {
			final JsonObject jsonObject = json.getAsJsonObject();

			String controlType = jsonObject.get("type").getAsString();

			Class clazz = CONTROLTYPE_CLASSES_MAP.get(controlType);
			if (clazz == null) {
				throw new RuntimeException(
						format("Unable to deserialize control JSON, unknown type %s", controlType));
			}

			return context.deserialize(jsonObject, clazz);
		}
	}

	private static class DateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
		public JsonElement serialize(Date date, Type typeOfSrc, JsonSerializationContext context) {
			if (date == null) {
				return JsonNull.INSTANCE;
			}

			return new JsonPrimitive(ISODateTimeFormat.dateTime().print(new DateTime(date)));
		}

		public Date deserialize(JsonElement json, Type typeOfSrc, JsonDeserializationContext context) throws JsonParseException {
			String str = json.getAsJsonPrimitive().getAsString();
			if (StringUtils.isNotEmpty(str)) {
				return ISODateTimeFormat.dateTime().parseDateTime(str).toDate();
			}

			return null;
		}
	}
}
