package org.dynaform.model;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Describes Form and it's surrounding classes
 */
public class Form {

	public String version;
	public String formName;
	public String formSymbolicName;
	public List<String> formHandlers = Lists.newArrayList();
	public List<FormControl> controls = Lists.newArrayList();

	/**
	 * Base for all Form Elements (Fields and Containers)
	 */
	public static class FormControl {
		public String name;
		public String label;
		public String type;

		public List<Validator> validators = Lists.newArrayList();
	}

	public static class Container extends FormControl {
		public Boolean optional;
		public List<FormControl> controls = Lists.newArrayList();
		public boolean horizontal;

		public Container() {
			this.type = FormControlType.CONTAINER.getTypeName();
		}
	}

	public static class RepeatingContainer extends Container {
		public final boolean repeat = true;

		public RepeatingContainer() {
			this.type = FormControlType.CONTAINER_REPEAT.getTypeName();
		}
	}

	public static class Field extends FormControl {
		public Object value;

		@Override
		public String toString() {
			return "Form: " + name;
		}
	}

	public static class FieldOptions extends Field implements HasOptions {
		public List<Option> classifier = Lists.newArrayList();

		@Override
		public void setOptions(Classifier classifier) {
			this.classifier = classifier.options;
		}
	}

	public static class FieldOptionsClassifier extends Field implements HasClassifierId {
		public String classifierId;

		@Override
		public String getClassifierId() {
			return classifierId;
		}
	}

	public static class FieldMultiselect extends Field {
		public List<Option> excluded = Lists.newArrayList();
	}

	public static class Option {
		public Object value;
		public String displayValue;

		public Option() {
		}

		public Option(Object value, String displayValue) {
			this.value = value;
			this.displayValue = displayValue;
		}

		@Override
		public String toString() {
			return "Option{" +
					"value=" + value +
					", displayValue=" + displayValue +
					'}';
		}
	}

	public interface HasOptions {
		void setOptions(Classifier classifier);
	}

	public interface HasClassifierId {
		String getClassifierId();
	}
}
