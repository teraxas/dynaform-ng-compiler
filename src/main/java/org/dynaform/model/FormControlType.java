package org.dynaform.model;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.Map;

/**
 * Represents relationships between FormControls and JSON 'type' values
 */
public enum FormControlType {

	CONTAINER("Container", Form.Container.class),
	CONTAINER_REPEAT("RepeatingContainer", Form.RepeatingContainer.class),

	FIELD("Field", Form.Field.class),
	FIELD_DATE("Date", Form.Field.class),
	FIELD_TEXTBOX("Textbox", Form.Field.class),
	FIELD_DROPDOWN("Dropdown", Form.FieldOptions.class),
	FIELD_DROPDOWN_CLASSIFIER("DropdownClassifier", Form.FieldOptions.class, FIELD_DROPDOWN),
	FIELD_LABEL("Label", Form.Field.class),
	FIELD_MULTISELECT("Multiselect", Form.FieldMultiselect.class),
	FIELD_TEXTBOX_CURRENCY("TextboxCurrency", Form.Field.class),
	FIELD_CHECKBOX("Checkbox", Form.Field.class);

	public static final ImmutableSet<FormControlType> CONTAINERS = ImmutableSet.of(
			CONTAINER,
			CONTAINER_REPEAT
	);
	public static final ImmutableSet<FormControlType> FIELDS = ImmutableSet.of(
			FIELD,
			FIELD_DATE,
			FIELD_TEXTBOX,
			FIELD_DROPDOWN,
			FIELD_LABEL,
			FIELD_MULTISELECT,
			FIELD_TEXTBOX_CURRENCY,
			FIELD_CHECKBOX
	);
	public static final Collection<FormControlType> REPLACEABLE_FIELDS = Collections2.filter(FIELDS, new Predicate<FormControlType>() {
		public boolean apply(FormControlType controlType) {
			return controlType.replacedBy != null;
		}
	});

	public static final Collection<String> CONTAINER_NAMES = Collections2.transform(CONTAINERS, new TypeNameExtractor());
	public static final Collection<String> FIELD_NAMES = Collections2.transform(FIELDS, new TypeNameExtractor());
	public static final Collection<String> REPLACEABLE_FIELD_NAMES = Collections2.transform(REPLACEABLE_FIELDS, new TypeNameExtractor());

	private final String typeName;
	private final Class formControlClass;

	private final FormControlType replacedBy;

	<T extends Form.FormControl> FormControlType(String typeName, Class<T> formControlClass) {
		this.typeName = typeName;
		this.formControlClass = formControlClass;
		this.replacedBy = null;
	}

	<T extends Form.FormControl> FormControlType(String typeName, Class<T> formControlClass, FormControlType replacedBy) {
		this.typeName = typeName;
		this.formControlClass = formControlClass;
		this.replacedBy = replacedBy;
	}

	/**
	 * Creates an instance of FormControl object, described by enum
	 */
	public Form.FormControl getNew() {
		Form.FormControl instance = instantiate();
		instance.type = this.typeName;
		return instance;
	}

	/**
	 * Checks if field type has Options
	 */
	public boolean hasOptions() {
		return Form.HasOptions.class.isAssignableFrom(this.formControlClass);
	}

	public String getTypeName() {
		return typeName;
	}

	public Class getFormControlClass() {
		return formControlClass;
	}

	public FormControlType getReplacedBy() {
		return replacedBy;
	}

	public static FormControlType getByTypeName(String typeName) {
		for (FormControlType fc : FormControlType.values()) {
			if (typeName.equals(fc.typeName)) {
				return fc;
			}
		}
		return null;
	}

	private Form.FormControl instantiate() {
		try {
			return (Form.FormControl) this.formControlClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static class TypeNameExtractor implements Function<FormControlType, String> {
		public String apply(FormControlType input) {
			return input.getTypeName();
		}
	}

	public static Map<String, Class> getControlTypesToClassesMap() {
		ImmutableMap.Builder<String, Class> mapBuilder = ImmutableMap.builder();

		FormControlType[] types = FormControlType.values();
		for (FormControlType type : types) {
			mapBuilder.put(type.getTypeName(), type.formControlClass);
		}

		return mapBuilder.build();
	}
}
