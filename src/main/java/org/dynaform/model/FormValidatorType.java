package org.dynaform.model;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.HashSet;

/**
 * Represents Validators and their supported components
 */
public enum FormValidatorType {
	REQUIRED("Required", Validator.class),
	REGEX("Regex", Validator.ValidatorRegex.class, FormControlType.FIELD_TEXTBOX),
	MAX_ROWS("MaxRows", Validator.ValidatorNumber.class, FormControlType.CONTAINER_REPEAT),
	MIN_ROWS("MinRows", Validator.ValidatorNumber.class, FormControlType.CONTAINER_REPEAT),
	MIN_LENGTH("MinLength", Validator.ValidatorNumber.class, FormControlType.FIELD_TEXTBOX),
	MAX_LENGTH("MaxLength", Validator.ValidatorNumber.class, FormControlType.FIELD_TEXTBOX)
	;

	public static final Collection<String> VALIDATOR_NAMES = Collections2.transform(Lists.newArrayList(FormValidatorType.values()), new TypeNameExtractor());
	private String typeName;

	private Class<? extends Validator> validatorClass;
	private final HashSet<FormControlType> supportedControls;

	/**
	 * If 'supportedControls' not provided - all controls are supported
	 */
	private <T extends Validator> FormValidatorType(String typeName, Class<T> validatorClass, FormControlType... supportedControls) {
		this.typeName = typeName;
		this.validatorClass = validatorClass;
		this.supportedControls = Sets.newHashSet(supportedControls == null || supportedControls.length == 0 ? supportedControls : FormControlType.values());
	}

	/**
	 * Creates an instance of Validator object, described by enum
	 */
	public Validator getNew() {
		Validator instance = instantiate();
		instance.type = this.typeName;
		return instance;
	}

	public boolean supportsControl(FormControlType controlType) {
		return this.supportedControls.contains(controlType);
	}

	private Validator instantiate() {
		try {
			return (Validator) this.validatorClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public String getTypeName() {
		return typeName;
	}

	public Class<? extends Validator> getValidatorClass() {
		return validatorClass;
	}

	private static class TypeNameExtractor implements Function<FormValidatorType, String> {
		public String apply(FormValidatorType input) {
			return input.getTypeName();
		}
	}

}