package org.dynaform.model;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Represents classifier JSON that provides Options for fields that require them.
 */
public class Classifier {

	public String classifierId;
	public List<Form.Option> options = Lists.newArrayList();

}
