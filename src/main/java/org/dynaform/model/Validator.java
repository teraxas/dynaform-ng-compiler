package org.dynaform.model;

public class Validator {

	public String type;

	public static class ValidatorRegex extends Validator {
		public String regex;
	}

	public static class ValidatorNumber extends Validator {
		public Integer number;
	}

}
