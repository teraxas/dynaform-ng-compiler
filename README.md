# Dynamic Forms

Java lib that contains utilities needed for server-side modification of dynamic forms:

* model classes and enums for building form descriptor
* validator
* forms compiler (transforms form, including classifiers)

## Maven artefact

```group: 'org.dynaform', name: 'dynaform-compiler'```